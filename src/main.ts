import 'hammerjs';
import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

import * as moment from 'moment';

moment.locale('pl');

if (environment.production) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));

const userId = localStorage.getItem('userId');
if (!userId) {
    localStorage.setItem('userId', (Math.random() * 100_000_000).toFixed(0));
    if (Math.random() > 0.5) {
        localStorage.setItem('rushHours', 'true');
    }

    if (Math.random() > 0.5) {
        localStorage.setItem('badAir', 'true');
    }
}
