import {Component, OnInit} from '@angular/core';
import {DrawCountService} from '../../../draw-count.service';
import * as moment from 'moment';

@Component({
  selector: 'app-winning-change-factors',
  templateUrl: './winning-chance-factors.component.html',
  styleUrls: ['./winning-chance-factors.component.scss']
})
export class WinningChanceFactorsComponent implements OnInit {
  constructor(private drawCountService: DrawCountService) {
  }

  ngOnInit() {
  }

  onScratched(active: boolean) {
    if (active) {
      this.drawCountService.increment();
    }
  }

  isRushHour() {
    return localStorage.getItem('rushHours');
  }

  isBadAir() {
    return localStorage.getItem('badAir');
  }

  getTime() {
    return moment().format('HH:mm');
  }
}
