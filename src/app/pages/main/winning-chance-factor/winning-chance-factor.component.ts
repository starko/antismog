import {Component, Input, NgZone, OnChanges, OnInit, Output} from '@angular/core';
import {SCRATCH_TYPE, ScratchCard} from '../../../scratch-card/ScratchCard';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-winning-chance-factor',
  templateUrl: './winning-chance-factor.component.html',
  styleUrls: ['./winning-chance-factor.component.scss']
})
export class WinningChanceFactorComponent implements OnInit, OnChanges {
  @Input() active: boolean;
  @Input() icon: string;
  @Input() subtitle: string;
  @Input() description: string;

  @Output() scratched = new Subject();

  constructor(private ngZone: NgZone) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    setTimeout(() => {
      const scContainer = document.querySelector(`.${this.icon}-scratch-card`) as HTMLDivElement;
      const scratchCard = new ScratchCard(`.${this.icon}-scratch-card`, {
        cursor: {
          cur: '',
          png: '',
          poosition: []
        },
        enabledPercentUpdate: true,
        scratchType: SCRATCH_TYPE.CIRCLE,
        containerWidth: scContainer.offsetWidth,
        containerHeight: scContainer.offsetHeight,
        imageForwardSrc: '/assets/scratchoff.png',
        brushSrc: '',
        imageBackgroundSrc: '',
        htmlBackground: '',
        clearZoneRadius: 20,
        percentToFinish: 80,
        nPoints: 30,
        pointSize: 2,
        callback: () => {
          if (this.active) {
            this.ngZone.run(() => this.scratched.next());
          }
        }
      });
      scratchCard.init();
    }, 0);
  }

}
