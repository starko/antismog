import {Component, OnInit} from '@angular/core';
import {UserInfoService} from '../../api/user-info.service';
import {Observable} from 'rxjs';
import {UserInfo} from '../../api/model/userInfo';
import {CityInfo} from '../../api/model/cityInfo';
import {CityInfoService} from '../../api/city-info.service';
import {DrawCountService} from '../../draw-count.service';
import {MatBottomSheet} from '@angular/material';
import {TicketChooserSheetComponent} from './ticket-chooser-sheet/ticket-chooser-sheet.component';
import {tap} from 'rxjs/operators';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
    userInfo: Observable<UserInfo>;
    cityInfo: Observable<CityInfo>;
    showCongrats: boolean;


    constructor(private cityInfoService: CityInfoService,
                private userInfoService: UserInfoService,
                public drawCountService: DrawCountService,
                private bottomSheet: MatBottomSheet) {
        this.cityInfo = this.cityInfoService.getCityInfo();
        this.userInfo = this.userInfoService.getUserInfo();
        this.drawCountService.onIncrement.pipe(
            tap(() => this.showCongrats = true),
            tap(() => setTimeout(() => this.showCongrats = false, 5000))
        ).subscribe();
    }

    ngOnInit() {
    }

    openBottomSheet() {
        this.bottomSheet.open(TicketChooserSheetComponent).afterDismissed();
    }
}
