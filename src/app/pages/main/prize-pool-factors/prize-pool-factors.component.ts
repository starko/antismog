import {Component, Input, OnInit} from '@angular/core';
import {CityInfo} from "../../../api/model/cityInfo";

@Component({
  selector: 'app-prize-pool-factors',
  templateUrl: './prize-pool-factors.component.html',
  styleUrls: ['./prize-pool-factors.component.scss']
})
export class PrizePoolFactorsComponent implements OnInit {

  @Input() cityInfo: CityInfo;

  constructor() { }

  ngOnInit() {
  }

}
