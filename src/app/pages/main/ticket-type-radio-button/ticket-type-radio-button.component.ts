import {Component, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {TicketType} from '../../../api/tickets.service';

@Component({
  selector: 'app-ticket-type-radio-button',
  templateUrl: './ticket-type-radio-button.component.html',
  styleUrls: ['./ticket-type-radio-button.component.scss']
})
export class TicketTypeRadioButtonComponent implements OnInit {
  @Input() type: TicketType;
  @Input() selected: boolean;
  @Input() reduced: boolean;
  @Output() clicked = new Subject();

  constructor() {
  }

  ngOnInit() {
  }

  getPriceString(): string {
    return this.getPrice().toFixed(2).replace('.', ',');
  }

  getPrice(): number {
    if (this.type === 'SINGLE_20') {
      return this.reduced ? 2.70 : 4.40;
    } else if (this.type === 'SINGLE_75') {
      return this.reduced ? 3.20 : 5.40;
    } else if (this.type === 'SINGLE_90') {
      return this.reduced ? 4.50 : 8;
    }
    return 0;
  }

  getName(): string {
    if (this.type === 'SINGLE_20') {
      return '20 min';
    } else if (this.type === 'SINGLE_75') {
      return '75 min';
    } else if (this.type === 'SINGLE_90') {
      return '90 min';
    }
    return '';
  }
}
