import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {DrawCountService} from '../../../draw-count.service';
import {MatBottomSheetRef} from '@angular/material';
import {TicketsService, TicketType} from '../../../api/tickets.service';
import {switchMap, tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-ticket-chooser-sheet',
  templateUrl: './ticket-chooser-sheet.component.html',
  styleUrls: ['./ticket-chooser-sheet.component.scss']
})
export class TicketChooserSheetComponent implements OnInit {
  ticketTypes: TicketType[] = [
    'SINGLE_20',
    'SINGLE_75',
    'SINGLE_90',
  ];

  selectedType: TicketType = 'SINGLE_20';
  reducedControl = new FormControl(false);

  constructor(public drawCountService: DrawCountService,
              private bottomSheetRef: MatBottomSheetRef<TicketChooserSheetComponent>,
              private ticketsService: TicketsService,
              private router: Router) {
  }

  ngOnInit() {
  }

  buyTicket() {
    this.ticketsService.buyTicket(this.selectedType, this.reducedControl.value, this.drawCountService.drawCount).pipe(
      tap(() => this.bottomSheetRef.dismiss()),
      switchMap(() => this.router.navigate(['/tickets']))
    ).subscribe();
  }
}
