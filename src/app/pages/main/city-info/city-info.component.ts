import {Component, OnInit} from '@angular/core';
import {CityInfoService} from '../../../api/city-info.service';
import {CityInfo} from '../../../api/model/cityInfo';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-city',
    templateUrl: './city-info.component.html',
    styleUrls: ['./city-info.component.scss']
})
export class CityInfoComponent implements OnInit {
    cityInfo: Observable<CityInfo>;

    constructor(private cityInfoService: CityInfoService) {
    }

    ngOnInit() {
        this.cityInfo = this.cityInfoService.getCityInfo();
    }

}
