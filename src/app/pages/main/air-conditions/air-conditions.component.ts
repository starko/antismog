import {Component, Input, OnInit} from '@angular/core';
import {CityInfo} from '../../../api/model/cityInfo';

@Component({
  selector: 'app-air-conditions',
  templateUrl: './air-conditions.component.html',
  styleUrls: ['./air-conditions.component.scss']
})
export class AirConditionsComponent implements OnInit {
  @Input() cityInfo: CityInfo;

  constructor() {
  }

  ngOnInit() {
  }

  getIcon(): string {
    if (this.cityInfo.airQuality.smogLevel === 'LOW') {
      return 'sentiment_satisfied';
    } else if (this.cityInfo.airQuality.smogLevel === 'MEDIUM') {
      return 'sentiment_dissatisfied';
    } else {
      return 'sentiment_very_dissatisfied';
    }
  }

  getDescription(): string {
    if (this.cityInfo.airQuality.smogLevel === 'LOW') {
      return 'Dobra jakość powietrza';
    } else if (this.cityInfo.airQuality.smogLevel === 'MEDIUM') {
      return 'Średnie zanieczyszczenie';
    } else {
      return 'Wysokie zanieczyszczenie';
    }
  }
}
