import {Component, Input, OnInit} from '@angular/core';
import {CityInfo} from "../../../api/model/cityInfo";

@Component({
  selector: 'app-prize-pool',
  templateUrl: './prize-pool.component.html',
  styleUrls: ['./prize-pool.component.scss']
})
export class PrizePoolComponent implements OnInit {
  @Input() cityInfo: CityInfo;

  constructor() {
  }

  ngOnInit() {
  }

}
