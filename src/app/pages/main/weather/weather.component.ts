import {Component, Input, OnInit} from '@angular/core';
import {CityInfo} from '../../../api/model/cityInfo';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  @Input() cityInfo: CityInfo;

  constructor() {
  }

  ngOnInit() {
  }

}
