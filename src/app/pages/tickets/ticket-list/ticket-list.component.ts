import {Component, OnInit} from '@angular/core';
import {TicketsService} from '../../../api/tickets.service';
import {Observable} from 'rxjs';
import {Ticket} from '../../../api/model/ticket';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit {
  tickets: Observable<Ticket[]>;
  totalDrawCount: Observable<number>;

  constructor(private ticketsService: TicketsService) {
    this.tickets = this.ticketsService.getTickets();
    this.totalDrawCount = this.tickets.pipe(
      map(tickets => tickets.reduce((prev: number, curr: Ticket) => prev + curr.multiplier, 0))
    );
  }

  ngOnInit() {
  }

  ticketTime(ticket: Ticket) {
    let ticketTimes = {
      'SINGLE_20': '20 min',
      'SINGLE_75': '75 min',
      'SINGLE_90': '90 min'
    }
    return ticketTimes[ticket.type];
  }

  getDrawCountString(drawCount: number): string {
    let drawString;

    if (drawCount === 1) {
      drawString = 'los';
    } else {
      drawString = 'losy';
    }

    return `${drawCount} ${drawString}`;
  }

}
