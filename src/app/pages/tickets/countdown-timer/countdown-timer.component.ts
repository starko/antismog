import {Component, OnInit} from '@angular/core';
import * as moment from 'moment';
import {interval, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-countdown-timer',
  templateUrl: './countdown-timer.component.html',
  styleUrls: ['./countdown-timer.component.scss']
})
export class CountdownTimerComponent implements OnInit {
  private seconds: Observable<string>;

  constructor() {
  }

  ngOnInit() {
    this.seconds = interval(1000).pipe(
      map(() => moment(moment().endOf('day').diff(moment())).format('ss'))
    );
  }

  getHoursLeft() {
    return moment(moment().endOf('day').diff(moment())).format('HH');
  }


  getMinutesLeft() {
    return moment(moment().endOf('day').diff(moment())).format('mm');
  }

  getSecondsLeft(): Observable<string> {
    return this.seconds;
  }
}
