/**
 * Return a random number in range
 */

export function randomPoint(min: number, max: number) {
  const random = Math.abs(Math.random() * (max - min) + min);
  return parseInt(random.toFixed(0), 10);
}

/**
 * Make a promise to load image
 */
export function loadImage(src: string) {
  return new Promise((resolve, reject) => {
    const image = new Image();
    image.crossOrigin = 'Anonymous'; // Work only if the server response headers contains [Access-Control-Allow-Origin: *]
    image.onload = () => {
      resolve(image);
    };
    image.src = src;
    image.onerror = (event: Event) => {
      const error = new Error(`Image ${src} is not loaded.`);
      reject(error);
    };
  });
}

export function throttle(callback: Function, delay: number) {
  let last: number;
  let timer: any;
  return function () {
    const context = this;
    const now: number = +new Date();
    const args = arguments;
    if (last && now < last + delay) {
      // le délai n'est pas écoulé on reset le timer
      clearTimeout(timer);
      timer = setTimeout(function () {
        last = now;
        callback.apply(context, args);
      }, delay);
    } else {
      last = now;
      callback.apply(context, args);
    }
  };
}

/*---- Events -----------------------------------------------------------------------*/
// polyfill source: https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
(function () {

  if (typeof (<any>window).CustomEvent === 'function') {
    return false;
  }

  function CustomEvent(event: any, params: any) {
    params = params || {bubbles: false, cancelable: false, detail: undefined};
    const evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
    return evt;
  }

  CustomEvent.prototype = (<any>window).Event.prototype;

  (<any>window).CustomEvent = CustomEvent;
})();

/**
 *
 */
export function dispatchCustomEvent(target: HTMLCanvasElement, type: string, detail: any) {
  const customEvent = new CustomEvent(type, {
    bubbles: true,
    cancelable: true,
    detail: detail
  });
  target.dispatchEvent(customEvent);
}

/**
 * Inject html behind the canvas
 */
export function injectHTML(html: string, target: HTMLElement) {
  const parser = new DOMParser();
  const wrapper = document.createElement('div');
  wrapper.classList.add('sc__inner');
  const content = parser.parseFromString(html, 'text/html'); // > IE 11
  wrapper.innerHTML = content.body.innerHTML;
  target.insertBefore(wrapper, target.firstElementChild);
}

/**
 * Get the real offset
 */
export function getOffset(element: HTMLElement) {
  const offset = {
    left: 0,
    top: 0
  };
  const clientRect = element.getBoundingClientRect();

  while (element) {
    offset.top += element.offsetTop;
    offset.left += element.offsetLeft;
    element = <HTMLElement>element.offsetParent;
  }

  // Calculate the delta between offset values and clientRect values
  const deltaLeft = offset.left - clientRect.left;
  const deltaTop = offset.top - clientRect.top;

  return {
    left: (deltaLeft < 0) ? offset.left + Math.abs(deltaLeft) : offset.left - Math.abs(deltaLeft),
    top: (deltaTop < 0) ? offset.top + Math.abs(deltaTop) : offset.top - Math.abs(deltaTop)
  };
}
