import {TicketType} from '../tickets.service';

export interface Ticket {
    qrCode?: string;
    validThru?: Date;
    buyTime?: Date;
    multiplier?: number;
    type: TicketType;
}
