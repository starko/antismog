export interface WeatherInfo {
  /**
   * temperatura
   */
  temp?: number;
  /**
   * opis np \"Pochmurno\"
   */
  kind?: string;
  /**
   * id ikony z open weather map
   */
  icon?: string;
}
