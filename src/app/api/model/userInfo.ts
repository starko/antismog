export interface UserInfo {
    chanceMultiplier?: number;

    currentLotteryValue?: number;
}
