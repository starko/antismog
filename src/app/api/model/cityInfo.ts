import {WeatherInfo} from './weatherInfo';
import {AirQuality} from './airQuality';

export interface CityInfo {
    name?: string;
    currentLotteryValue?: number;
    airQuality?: AirQuality;
    weather?: WeatherInfo;
    currentTime?: Date;
    weatherDotation: number;
    smogDotation: number;
}

