import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {WeatherInfo} from './model/weatherInfo';

@Injectable({
  providedIn: 'root'
})
export class WeatherInfoService {

  constructor(private http: HttpClient) {
  }

  getWeatherInfo(): Observable<WeatherInfo> {
    return this.http.get<WeatherInfo>(`${environment.apiUrl}/weatherInfo`);
  }
}
