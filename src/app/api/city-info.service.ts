import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CityInfo} from './model/cityInfo';
import {environment} from '../../environments/environment';
import {interval, Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CityInfoService {

    constructor(private http: HttpClient) {
    }

    getCityInfo(): Observable<CityInfo> {
        return interval(2000).pipe(
            switchMap(() => this.http.get<CityInfo>(`${environment.apiUrl}/cityInfo`))
        );
    }
}
