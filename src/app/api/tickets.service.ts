import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Ticket} from './model/ticket';

@Injectable({
    providedIn: 'root'
})
export class TicketsService {

    constructor(private http: HttpClient) {
    }

    getTickets(): Observable<Ticket[]> {
        return this.http.get<Ticket[]>(`${environment.apiUrl}/tickets`, {params: {userId: localStorage.getItem('userId')}});
    }

    buyTicket(ticketType: TicketType, reduced: boolean, drawCount: number): Observable<void> {
        return this.http.post<void>(`${environment.apiUrl}/buy-ticket`, {
            ticketType,
            reduced,
            drawCount,
            userId: localStorage.getItem('userId')
        });
    }
}

export type TicketType = 'SINGLE_20' | 'SINGLE_75' | 'SINGLE_90';
