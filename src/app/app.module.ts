import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {MainComponent} from './pages/main/main.component';
import {AppComponent} from './app.component';
import {CityInfoComponent} from './pages/main/city-info/city-info.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PrizePoolComponent} from './pages/main/prize-pool/prize-pool.component';
import {WeatherComponent} from './pages/main/weather/weather.component';
import {PrizePoolFactorsComponent} from './pages/main/prize-pool-factors/prize-pool-factors.component';
import {AirConditionsComponent} from './pages/main/air-conditions/air-conditions.component';
import {WinningChanceComponent} from './pages/main/winning-chance/winning-chance.component';
import {WinningChanceFactorsComponent} from './pages/main/winning-chance-factors/winning-chance-factors.component';
import {WinningChanceFactorComponent} from './pages/main/winning-chance-factor/winning-chance-factor.component';
import {TicketsComponent} from './pages/tickets/tickets.component';
import {Ng2OdometerModule} from 'ng2-odometer';
import {TicketChooserSheetComponent} from './pages/main/ticket-chooser-sheet/ticket-chooser-sheet.component';
import {TicketTypeRadioButtonComponent} from './pages/main/ticket-type-radio-button/ticket-type-radio-button.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CountdownTimerComponent} from './pages/tickets/countdown-timer/countdown-timer.component';
import {TicketListComponent} from './pages/tickets/ticket-list/ticket-list.component';
import {MomentModule} from 'ngx-moment';

@NgModule({
    declarations: [
        MainComponent,
        CountdownTimerComponent,
        AppComponent,
        CityInfoComponent,
        PrizePoolComponent,
        WeatherComponent,
        PrizePoolFactorsComponent,
        AirConditionsComponent,
        WinningChanceComponent,
        WinningChanceFactorsComponent,
        WinningChanceFactorComponent,
        TicketsComponent,
        TicketChooserSheetComponent,
        TicketTypeRadioButtonComponent,
        TicketListComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialModule,
        FlexLayoutModule,
        Ng2OdometerModule.forRoot(),
        ReactiveFormsModule,
        MomentModule
    ],
    entryComponents: [TicketChooserSheetComponent],
    bootstrap: [AppComponent]
})
export class AppModule {
}
