import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatBottomSheetModule,
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';

@NgModule({
  declarations: [],
  exports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatRippleModule,
    MatToolbarModule,
    MatBottomSheetModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule
  ],
})
export class MaterialModule {
}
