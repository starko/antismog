import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DrawCountService {
  public drawCount = 1;
  public onIncrement = new Subject();

  constructor() {
  }

  getDrawCountString(): string {
    let drawString;

    if (this.drawCount === 1) {
      drawString = 'los';
    } else {
      drawString = 'losy';
    }

    return `${this.drawCount} ${drawString}`;
  }

  increment() {
    this.onIncrement.next();
    this.drawCount++;
  }
}
